# Simple React/Redux Game #


### What is this repository for? ###

* Application created as part of a quiz for children.
* Version: 0.1.0


### How to start ###

* To install all node-modules ran:
    ```bash
      yarn
    ```
* To start app:
  ```bash
    yarn start

### Contact ###

* Štrbo Miloš
* strbo89@yahoo.com